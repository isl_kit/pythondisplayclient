#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
from os.path import exists
import json

from twisted.internet import reactor

from pythondisplayclient.logging import Logger
from pythondisplayclient.socket_client import SocketClientFactory
from pythondisplayclient.unixIn import UnixInFactory
from pythondisplayclient.unixOut import UnixOutFactory
from pythondisplayclient import settings

def main():
    client_factory = SocketClientFactory()
    unix_in_factory = UnixInFactory(client_factory)
    unix_out_factory = UnixOutFactory()
    client_factory.set_unix_factory(unix_out_factory)
    for source in settings.SOURCES:
        reactor.connectTCP(source['host'], source['port'], client_factory)
    reactor.listenUNIX(settings.UNIX_OUT_SOCKET, unix_out_factory)
    reactor.listenUNIX(settings.UNIX_IN_SOCKET, unix_in_factory)
    reactor.run()


if __name__ == '__main__':
    main()
