from distutils.core import setup

setup(
    name='PythonDisplayClient',
    version='2.0.0',
    author='Bastian Krüger',
    author_email='bastian.krueger@kit.edu',
    packages=['pythondisplayclient'],
    scripts=['bin/pdc_connect.py'],
    url='https://bitbucket.org/isl_kit/pythondisplayclient',
    license='LICENSE.txt',
    description='A small python tool, that connects as DisplayServer to one or multiple Mediators.',
    long_description=open('README.md').read(),
    install_requires=[
        'python-dotenv>=0.6.4',
        'environs>=1.2.0',
        'twisted>=17.5.0',
        'python-dateutil>=2.6.0'
    ],
    zip_safe = False,
)
