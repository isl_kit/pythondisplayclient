#!/usr/bin/env python
# -*- coding: utf-8 -*-

from typing  import Dict
from .helper import get_stream_id


class SessionInput(object):

    def __init__(self, fingerprint, inp_type):
        self.fingerprint = fingerprint
        self.inp_type = inp_type

    def to_dict(self):
        return {
            'fingerprint': self.fingerprint,
            'type': self.inp_type
        }


class SessionSource(object):

    def __init__(self, host, port):
        self.host = host
        self.port = port

    def to_dict(self):
        return {
            'host': self.host,
            'port': self.port
        }

    def __str__(self):
        return '%s:%d' % (self.host, self.port)


class Session(object):
    items: Dict[int, 'Session'] = {}

    def __init__(self, id, title, description, input_fingerprint, password, start, source, audio_file):
        self.id: int = id
        self.title: str = title
        self.description: str = description
        self.input = SessionInput(input_fingerprint, 'audio')
        self.password: str = password
        self.start = start
        self.source = source
        self.streams = {}
        self.transcript_stream = None
        self.audio_file = audio_file
        Session.items[id] = self

    def to_dict(self):
        return {
            'id': self.id,
            'title': self.title,
            'desc': self.description,
            'input': self.input.to_dict(),
            'password': self.password,
            'source': self.source.to_dict(),
            'streams': [stream.to_dict() for _, stream in self.streams.items()],
            'audio_file': self.audio_file
        }

    def set_transcript_stream(self):
        first_input_part = self.input.fingerprint.split('-')[0].lower()
        res = [stream for _, stream in self.streams.items() if stream.type == 'text' and stream.fingerprint.split('-')[0].lower() == first_input_part]
        try:
            self.transcript_stream = res[0]
        except IndexError:
            self.transcript_stream = None

    def delete(self):
        del Session.items[self.id]

    def has_stream(self, stream_id):
        return stream_id in self.streams

    def add_stream(self, stream):
        self.streams[stream.id] = stream

    def get_stream(self, stream_id):
        return self.streams.get(stream_id, None)


class Stream(object):

    def __init__(self, session, type, sid, fingerprint, displayname, control):
        self.id = 0
        self.session = session
        self.type = type
        self.sid = sid
        self.fingerprint = fingerprint
        self.displayname = displayname
        self.control = control
        self.data = []
        self.path = []

    def to_dict(self):
        return {
            'id': self.id,
            'type': self.type,
            'sid': self.sid,
            'fingerprint': self.fingerprint,
            'displayname': self.displayname,
            'control': self.control,
            'path': [step.to_dict() for step in self.path]
        }

    def set_id(self):
        self.id = get_stream_id(self.fingerprint, self.type)

    def add_data(self, data):
        self.data.append(data)

    def add_to_path(self, step):
        self.path.append(step)


class Data(object):

    def __init__(self, start_timestamp, stop_timestamp, stream, startoffset, stopoffset, text, creator):
        self.stream = stream
        self.start_timestamp = start_timestamp
        self.stop_timestamp = stop_timestamp
        self.startoffset = startoffset
        self.stopoffset = stopoffset
        self.text = text
        self.creator = creator

    def to_dict(self):
        return {
            'stream_id': self.stream.id,
            'session_id': self.stream.session.id,
            'fingerprint': self.stream.fingerprint,
            'type': self.stream.type,
            'start_timestamp': self.start_timestamp,
            'stop_timestamp': self.stop_timestamp,
            'startoffset': self.startoffset,
            'stopoffset': self.stopoffset,
            'text': self.text,
            'creator': self.creator
        }


class Step(object):

    def __init__(self, fingerprint, type, name, node):
        self.fingerprint = fingerprint
        self.type = type
        self.name = name
        self.node = node

    def to_dict(self):
        return {
            'fingerprint': self.fingerprint,
            'type': self.type,
            'name': self.name,
            'node': self.node.to_dict() if self.node else None
        }


class Node(object):

    def __init__(self, name, creator, input_fingerprint, input_type, output_fingerprint, output_type):
        self.name = name
        self.creator = creator
        self.input_fingerprint = input_fingerprint
        self.input_type = input_type
        self.output_fingerprint = output_fingerprint
        self.output_type = output_type

    def to_dict(self):
        return {
            'name': self.name,
            'creator': self.creator,
            'input_fingerprint': self.input_fingerprint,
            'input_type': self.input_type,
            'output_fingerprint': self.output_fingerprint,
            'output_type': self.output_type
        }
