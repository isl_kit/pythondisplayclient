#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json

from twisted.internet.protocol import ServerFactory
from twisted.protocols.basic import LineReceiver

from .helper import get_stream_id


class UnixInProtocol(LineReceiver):

    MAX_LENGTH = 64*1024*1024

    def __init__(self, client_factory):
        print('INIT')
        self.setLineMode()
        self.client_factory = client_factory

    def lengthLimitExceeded(self, length):
        print('EXCEED')
        print(length)

    def lineReceived(self, data):
        try:
            data = data.decode("utf-8", "strict")
        except Exception as e:
            print("Warning! Can not decode message as utf8. Sending possibly garbled data. Error follows.")
            print(e)
            data = data.decode("utf-8", "replace")
        self.msg_handler(json.loads(data))

    def msg_handler(self, msg):
        if msg['action'] == 'join':
            for _, protocol in self.client_factory.protocols.items():
                protocol.handle_join(msg['client_id'], msg['session_id'], msg['stream_id'])
        elif msg['action'] == 'leave':
            for _, protocol in self.client_factory.protocols.items():
                protocol.handle_leave(msg['client_id'], msg['session_id'], msg['stream_id'])
        elif msg['action'] == 'disconnect':
            for _, protocol in self.client_factory.protocols.items():
                protocol.handle_disconnect(msg['client_id'])


class UnixInFactory(ServerFactory):

    def __init__(self, client_factory):
        self.client_factory = client_factory

    def buildProtocol(self, addr):
        return UnixInProtocol(self.client_factory)
