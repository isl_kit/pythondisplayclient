#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json

from twisted.internet.protocol import ServerFactory, Protocol


class UnixOutProtocol(Protocol):

    def __init__(self, factory):
        self.factory = factory

    def connectionMade(self):
        self.factory.clients.append(self)
        for getter in self.factory.callbacks:
            data = getter()
            if data:
                self.transport.write((json.dumps(data)+'\r\n').encode())

    def connectionLost(self, reason):
        self.factory.clients.remove(self)


class UnixOutFactory(ServerFactory):

    def __init__(self):
        self.clients = []
        self.callbacks = []

    def broadcast(self, data):
        for client in self.clients:
            print('broadcast {}'.format(json.dumps(data)))
            client.transport.write((json.dumps(data)+'\r\n').encode())

    def broadcastOnConnection(self, getter):
        if getter not in self.callbacks:
            self.callbacks.append(getter)

    def buildProtocol(self, addr):
        return UnixOutProtocol(self)
