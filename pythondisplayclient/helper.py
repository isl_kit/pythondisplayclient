#!/usr/bin/env python
# -*- coding: utf-8 -*-


def get_stream_id(fingerprint, stream_type):
    return '{}_{}'.format(fingerprint, stream_type)


def get_stream_params_from_id(id):
    parts = id.split('_')
    return {
        'fingerprint': parts[0],
        'sid': parts[1],
        'type': parts[2]
    }
