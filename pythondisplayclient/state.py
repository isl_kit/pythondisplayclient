#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json


class State(object):

    def __init__(self, path):
        self.path = path
        try:
            with open(path, 'r') as f:
                self.state = json.load(f)
        except:
            self.state = {}

    def get_state(self, source, key):
        if source not in self.state:
            return None
        if key not in self.state[source]:
            return None
        return self.state[source][key]

    def set_state(self, source, key, val):
        if source not in self.state:
            self.state[source] = {}
        if type(val) is str:
            print(val, type(val))
        self.state[source][key] = val
        with open(self.path, 'w+') as f:
            json.dump(self.state, f)

    def remove_state(self, source, key):
        if source not in self.state:
            return
        if key not in self.state[source]:
            return
        del self.state[source][key]
        with open(self.path, 'w+') as f:
            json.dump(self.state, f)
