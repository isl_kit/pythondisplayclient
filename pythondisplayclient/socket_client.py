#!/usr/bin/env python
# -*- coding: utf-8 -*-

from twisted.internet.protocol import ReconnectingClientFactory
from twisted.protocols.basic import Int32StringReceiver

from . import settings
from .logging import Logger
from .models import Session, SessionSource
from .msg_handler import MsgHandler

def _conn_to_str(connector):
    """
    Returns a better string of the destinationfor a twisted connector.
    """
    conn = connector.getDestination()
    return f"{conn.host}:{conn.port}"

class SocketClientProtocol(Int32StringReceiver):

    MAX_LENGTH = 64*1024*1024

    def __init__(self, addr, unix_factory):
        self.logger = Logger(settings.LOG_DIR)
        self.unix_factory = unix_factory
        self.source = SessionSource(addr.host, addr.port)
        self.registered_streams = {}
        self.msg_handler = MsgHandler(self)
        super().__init__()

    def handle_disconnect(self, client_id):
        for session_id, streams in self.registered_streams.items():
            stream_ids = [stream_id for stream_id in streams]
            for stream_id in stream_ids:
                self.handle_leave(client_id, session_id, stream_id)

    def handle_join(self, client_id, session_id, stream_id):
        if session_id not in Session.items:
            return
        session = Session.items[session_id]
        if session.source != self.source:
            return
        stream = session.get_stream(stream_id)
        if not stream:
            return
        if session_id not in self.registered_streams:
            self.registered_streams[session_id] = {}
        if stream_id not in self.registered_streams[session_id]:
            self.connect_to_stream(stream)
            self.registered_streams[session_id][stream_id] = []
        if client_id not in self.registered_streams[session_id][stream_id]:
            self.registered_streams[session_id][stream_id].append(client_id)

    def handle_leave(self, client_id, session_id, stream_id):
        if session_id not in self.registered_streams:
            return
        if stream_id not in self.registered_streams[session_id]:
            return
        if client_id not in self.registered_streams[session_id][stream_id]:
            return
        self.registered_streams[session_id][stream_id].remove(client_id)
        if len(self.registered_streams[session_id][stream_id]) == 0:
            if session_id in Session.items:
                session = Session.items[session_id]
                stream = session.get_stream(stream_id)
                if stream:
                    self.disconnect_from_stream(stream)
            del self.registered_streams[session_id][stream_id]

    def broadcast(self, data):
        self.logger.log_broadcast(data['type'], data['data'])
        self.unix_factory.broadcast(data)

    def lengthLimitExceeded(self, length):
        print(f'[{self.source}] EXCEED {length}')

    def stringReceived(self, msg):
        self.msg_handler.handle_msg(msg)

    def get_register_xml(self, rtype, stream):
        return (
            '<register type="%s" sessionid="%d"><streams>'
            '<stream type="%s" fingerprint="%s" displayname="%s" '
            'control="%s" streamid="%s"></stream></streams></register>'
        ) % (rtype, stream.session.id, stream.type, stream.fingerprint,
             stream.displayname, stream.control, stream.sid)

    def connect_to_stream(self, stream):
        print('connect to stream {}!'.format(stream.id))
        print(type(stream), stream)
        self.sendString(self.get_register_xml('add', stream).encode())

    def disconnect_from_stream(self, stream):
        print('disconnect from stream {}!'.format(stream.id))
        self.sendString(self.get_register_xml('delete', stream).encode())


class SocketClientFactory(ReconnectingClientFactory):

    def __init__(self):
        self.protocols = {}
        self.unix_factory = None
        super().__init__()

    def startedConnecting(self, connector):
        print(f'[{_conn_to_str(connector)}] Started to connect.')

    def buildProtocol(self, addr):
        print(f'[{addr.host}:{addr.port}] Connected.')
        self.resetDelay()
        protocol = SocketClientProtocol(addr, self.unix_factory)
        self.protocols[str(protocol.source)] = protocol
        return protocol

    def clientConnectionLost(self, connector, reason):
        print(f'[{_conn_to_str(connector)}] Lost connection.  Reason:', reason.getErrorMessage())
        ReconnectingClientFactory.clientConnectionLost(self, connector, reason)

    def clientConnectionFailed(self, connector, reason):
        print(f'[{_conn_to_str(connector)}] Connection failed. Reason:', reason.getErrorMessage())
        ReconnectingClientFactory.clientConnectionFailed(self, connector, reason)

    def set_unix_factory(self, unix_factory):
        self.unix_factory = unix_factory
        unix_factory.broadcastOnConnection(self.get_all_sessions)

    def get_all_sessions(self):
        sessions = [session.to_dict() for _, session in Session.items.items()]
        if len(sessions) == 0:
            return None
        return {
            'type': 0,
            'data': sessions
        }
