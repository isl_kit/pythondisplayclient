#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

from dotenv import load_dotenv
from environs import Env

env = Env()
load_dotenv('.env')

SOURCES = env.json('PDC_SOURCES', '[{"host": "141.3.25.30","port": 4444},{"host": "141.3.25.53","port": 4444}]')
UNIX_IN_SOCKET = env('PDC_UNIX_IN_SOCKET', '/tmp/unix_in.sock')
UNIX_OUT_SOCKET = env('PDC_UNIX_OUT_SOCKET', '/tmp/unix_out.sock')
SESSION_STATE = env('PDC_SESSION_STATE', '/tmp/pdc_state.json')
LOG_DIR = env('PDC_LOG_DIR', '/tmp/pdc_log')
BLACKLIST = env.list('PDC_BLACKLIST', ['Single translation'])
