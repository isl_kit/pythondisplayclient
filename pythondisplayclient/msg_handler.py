#!/usr/bin/env python
# -*- coding: utf-8 -*-

import xml.etree.ElementTree as ET
from datetime import datetime
from dateutil.parser import parse
from urllib.parse import unquote

from . import settings
from .db_handler import DbHandler
from .state import State
from .logging import Logger


class MsgHandler(object):

    def __init__(self, socket):
        self.socket = socket
        self.state = State(settings.SESSION_STATE)
        self.logger = Logger(settings.LOG_DIR)
        self.db_handler = DbHandler(socket)

    def handle_msg(self, msg):
        msg = msg.decode()
        #print("Handling message", msg)
        et_data = ET.fromstring(msg)
        self.handle_element(et_data, msg)

    def handle_element(self, el, raw_message):
        if el.tag == 'session' and el.get('type') == 'add':
            self.handle_start_session(el, raw_message)
        elif el.tag == 'session' and el.get('type') == 'delete':
            self.handle_end_session(el)
        elif el.tag == 'data':
            self.handle_data(el)
        else:
            print("Unknown xml tag found while handling msg:", el.tag)

    def handle_start_session(self, el, raw_message):
        self.logger.log_session_start(el)
        sessionid = int(el.get('sessionid'))
        session_info = el.find('sessioninfos').find('sessioninfo')
        title = unquote(session_info.get('name'))
        if title in settings.BLACKLIST:
            return
        state = self.state.get_state(str(self.socket.source), sessionid)
        if state is not None:
            self.db_handler.create_session(ET.fromstring(state['raw']), start=parse(state['start']))
        else:
            session = self.db_handler.create_session(el, datetime.now())
            if session is not None:
                self.state.set_state(str(self.socket.source), sessionid, {
                    'raw': raw_message,
                    'start': session.start.isoformat()
                })

    def handle_end_session(self, el):
        self.logger.log_session_end(el)
        sessionid = int(el.get('sessionid'))
        self.state.remove_state(str(self.socket.source), sessionid)
        self.db_handler.delete_session(sessionid)

    def handle_data(self, el):
        self.logger.log_data(el)
        self.db_handler.create_data(el)
