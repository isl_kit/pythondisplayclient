#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os import listdir
from os.path import join, exists
from urllib.parse import unquote
from datetime import datetime
import time

from .models import Session, Stream, Data, Step, Node
from .helper import get_stream_id


class DbHandler(object):

    def __init__(self, socket):
        self.socket = socket

    def create_step(self, step_el):
        return Step(step_el.get('ifp'), step_el.get('itype'), step_el.get('name'), self.create_node(step_el.find('node')))

    def create_node(self, node_el):
        if node_el is None:
            return None
        return Node(node_el.get('name'), node_el.get('creator'), node_el.get('ifp'), node_el.get('itype'), node_el.get('ofp'), node_el.get('otype'))

    def create_session(self, session_el, start):
        sessionid = int(session_el.get('sessionid'))
        password = session_el.get('password')
        session_info = session_el.find('sessioninfos').find('sessioninfo')
        streams_el = session_el.find('streams').findall('stream')
        title = unquote(session_info.get('name'))
        description = unquote(session_info.get('description'))
        input_fingerprint = unquote(session_info.get('language'))
        audio_file = unquote(session_info.get('audiopath', default=''))
        audio_file = audio_file if audio_file else ''

        if len(streams_el) == 0:
            return None

        session = Session(sessionid, title, description, input_fingerprint, password, start, self.socket.source, audio_file)

        for stream_el in streams_el:
            stream = self.create_stream(session, stream_el)
            if stream:
                session.add_stream(stream)
        session.set_transcript_stream()

        if session.transcript_stream:
            self.socket.handle_join('c' + str(session.id), session.id, session.transcript_stream.id)
        self.socket.broadcast({
            'type': 0,
            'data': session.to_dict()
        })
        return session

    def delete_session(self, sessionid):
        if sessionid not in Session.items:
            return
        session = Session.items[sessionid]
        session.delete()
        self.socket.broadcast({
            'type': 1,
            'data': {
                'id': sessionid
            }
        })

    def create_stream(self, session, stream_el):
        fingerprint = stream_el.get('fingerprint')
        displayname = stream_el.get('displayname')
        sid = stream_el.get('streamid')
        control = stream_el.get('control')
        stream_type = stream_el.get('type')
        path_el = stream_el.find('path')

        # ignore all streams that are not text (no audio, no unseg-text, ...) and not backchannel
        if stream_type != 'text' and stream_type != 'unseg-text' and stream_type != 'backchannel':
            return None

        stream = Stream(session, stream_type, sid, fingerprint, displayname, control)
        if path_el is not None:
            for step_el in path_el.findall('step'):
                stream.add_to_path(self.create_step(step_el))
        stream.set_id()
        return stream

    def get_date(self, date_str):
        formats = ['%d/%m/%y-%H:%M:%S.%f', '%d-%m-%y %H:%M:%S.%f']
        for format in formats:
            try:
                return datetime.strptime(date_str, format)
            except:
                pass
        return datetime.now()

    def create_data(self, data_el):
        sessionid = int(data_el.get('sessionid'))
        fingerprint = data_el.get('fingerprint')
        sid = data_el.get('streamid')
        stream_type = data_el.get('type')
        creator = data_el.get('creator')
        startoffset = int(data_el.get('startoffset', data_el.get('timeoffset', 0)))
        stopoffset = int(data_el.get('stopoffset', startoffset + 500))
        start = data_el.get('start')
        stop = data_el.get('stop')
        text = data_el.find('text').text
        wt = data_el.find('wordtokens')

        if wt is not None and stream_type == 'text' and fingerprint not in ['mx', 'mu']:
            stream_type = 'unseg-text'

        # URL decoding
        text = unquote(text) if text else ''

        if sessionid not in Session.items:
            return None
        session = Session.items[sessionid]

        stream_id = get_stream_id(fingerprint, stream_type)
        stream = session.get_stream(stream_id)
        if not stream:
            return None

        # parse datestrings into datetime objects ('000' is appended because %f expects microseconds but miliseconds given)
        start_date = self.get_date(start + '000')
        stop_date = self.get_date(stop + '000')

        start_timestamp = int(time.mktime(start_date.timetuple()) * 1000 + (start_date.microsecond / 1000))
        stop_timestamp = int(time.mktime(stop_date.timetuple()) * 1000 + (stop_date.microsecond / 1000))

        if start_timestamp < 0:
            start_timestamp = startoffset
            stop_timestamp = stopoffset

        last_data = stream.data[-1] if len(stream.data) > 0 else None

        # fix for <br> messages, so that they don't have same startoffset as other messages
        if text.startswith('<br>'):
            if last_data is None or start_timestamp - last_data.start_timestamp > 1:
                start_timestamp = start_timestamp - 1
                stop_timestamp = stop_timestamp - 1

        if last_data and start_timestamp - last_data.start_timestamp < 0:
                print("WARNING! Got message with timestamp older than last one.")
                return None

        # workaround for unwanted small delays:
        # if difference is max 100ms and start if before last stop, we still use the last data element
        # if last_data and (start_timestamp == last_data.start_timestamp or (start_timestamp - last_data.start_timestamp <= 100 and start_timestamp < last_data.stop_timestamp)):
        #     if last_data.text == text:
        #         return None
        #     data = last_data
        #     data.text = text
        #     data.start = start_date
        #     data.stop = stop_date
        #     data.startoffset = startoffset
        #     data.stopoffset = stopoffset
        #     data.stop_timestamp = stop_timestamp
        # else:
        data = Data(start_timestamp, stop_timestamp, stream, startoffset, stopoffset, text, creator)
        stream.add_data(data)

        self.socket.broadcast({
            'type': 2,
            'data': data.to_dict()
        })
        return data
