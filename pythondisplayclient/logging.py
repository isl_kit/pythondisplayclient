#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import os.path
from datetime import datetime


class Logger(object):
    def __init__(self, log_dir):
        self.log_dir = log_dir
        try:
            if not os.path.exists(self.log_dir):
                os.makedirs(self.log_dir)
        except:
            pass

    def log_session_start(self, el):
        self.log(el.get('sessionid'), 'SESSION START')

    def log_data(self, el):
        creator = el.get('creator')
        startoffset = int(el.get('startoffset', el.get('timeoffset', 0)))
        stopoffset = int(el.get('stopoffset', startoffset + 500))
        start = el.get('start')
        stop = el.get('stop')
        text = el.find('text').text
        wt = el.find('wordtokens')
        lang = el.get('fingerprint').split("-", 1)[0]
        msg = '[{}/{}] ({} / {}) : ({} / {}) / {}'.format(creator, lang, start, stop, startoffset, stopoffset, text)
        if wt is not None:
            for token in wt.findall('wordtoken'):
                t_start = token.get('starttime')
                t_stop = token.get('stoptime')
                t_text = token.text
                msg += '\n\tTOKEN: {} / {} / {}'.format(t_start, t_stop, t_text)
        self.log(el.get('sessionid'), msg)

    def log_broadcast(self, btype, data):
        if btype != 2:
            return
        session_id = data['session_id']
        text = data['text']
        msg = 'BROADCAST ({})'.format(text)
        self.log(session_id, msg)

    def log_session_end(self, el):
        self.log(el.get('sessionid'), 'SESSION END\n')

    def log(self, session_id, message):
        now = datetime.now()
        msg = '{}:: {}'.format(now.strftime('%H:%M:%S.%f'), message)
        logfile = os.path.join(self.log_dir, '{}.log'.format(session_id))
        try:
            with open(logfile, 'a+') as f:
                f.write(msg + '\n')
        except:
            pass
