# Python DisplayClient

The Python DisplayClient connects as DisplayClient to one or multiple Mediators and writes the results to a Unix Socket. A second Unix Socket accepts requests to connect to certain streams.

## Install

    pip install git+https://bitbucket.org/isl_kit/pythondisplayclient.git
or
    pip install git+ssh://git@bitbucket.org/isl_kit/pythondisplayclient.git

## Usage

### Set configuration

Ensure that you have set all necessary environment variables or created a `.env` file, containing those values (see [Config](#config)).

### Connect to Mediator

    pdc_connect.py

After connecting to the Mediators, the Python DisplayClient will open two Unix sockets:

- PDC_UNIX_OUT_SOCKET (default='/tmp/unix_out.sock')
- PDC_UNIX_IN_SOCKET (default='/tmp/unix_in.sock')

Applications connected to the Python DisplayClient will receive incoming messages from the Mediator on `PDC_UNIX_OUT_SOCKET` (i.e. start of session) and will be able send commands back to the Mediator via `PDC_UNIX_IN_SOCKET` (i.e. connect to stream `de_text`). A detailed description on how those messages look like can be found in [Messages](#messages).

## Messages

### Incoming messages

There three different types of incoming messages:

- SessionStart: when a session is started on the Mediator
- SessionEnd: when a session has stopped on the Mediator
- Data: data sent from one of the connected streams

#### SessionStart

    {
        'type': 0,
        'data': {
            'id': 123456789,
            'title': 'TestSession',
            'desc': 'a description of the session',
            'input': {
                'fingerprint': 'de-DE-lecture_KIT',
                'type': 'audio'
            },
            'password': 'secret',
            'source': {
                'host': '141.3.25.30',
                'port': 4444
            },
            'streams': [
                {
                    'id': 'de_text', // format: fingerprint_type
                    'type': 'text',
                    'sid': 'speech',
                    'fingerprint': 'de',
                    'displayname': 'German text',
                    'control': '*',
                    'path': [ //defines all nodes/workers that were used to create this stream
                        {
                            'fingerprint': 'de',
                            'type': 'unseg_text',
                            'name': '...',
                            'node': {
                                'name': 'asr',
                                'creator': 'asr worker',
                                'input_fingerprint': 'de-DE-lecture_KIT',
                                'input_type': 'audio',
                                'output_fingerprint': 'de',
                                'output_type': 'unseg_text'
                            }
                        },
                        {
                            //...
                        }
                    ]
                },
                {
                    //...
                }
            ],
            'audio_file': '/path/to/raw/audio/file.raw'
        }
    }

#### SessionEnd

    {
        'type': 1,
        'data': {
            'id': 123456789
        }
    }

#### Data

    {
        'type': 2,
        'data': {
            'stream_id': 'de_text',
            'session_id': 123456789,
            'fingerprint': 'de',
            'type': 'text',
            'start_timestamp': 23461134356,
            'stop_timestamp': 23461134663,
            'startoffset': 2355,
            'stopoffset': 2662,
            'text': 'war ja auch ein besonders guter...',
            'creator': 'seg-v2'
        }
    }


### Outgoing messages

In order to receive any data from a session, the connected application has to subscribe to a stream first. This is done by sending JSON formatted messages to the PDC_UNIX_IN_SOCKET. The messages have to be of the following format:

#### Join a stream

    {
        'action': 'join',
        'client_id': '429abdg902sd0234u08432',
        'session_id': 123456789,
        'stream_id': 'de_text'
    }

#### Leave a stream

    {
        'action': 'leave',
        'client_id': '429abdg902sd0234u08432',
        'session_id': 123456789,
        'stream_id': 'de_text'
    }

#### Disconnect a client

    {
        'action': 'disconnect',
        'client_id': '429abdg902sd0234u08432'
    }

## Config

Configuration is done with environment variables. Those variables can also be defined in a `.env` file, located in the directory from which `pdc_connect.py` is called.

The following shows a list of all available settings along with their default values.

### PDC_SOURCES

An array of objects, defining all available Mediators:

    PDC_SOURCES=[{"host": "141.3.25.30","port": 4444},{"host": "141.3.25.53","port": 4444}]

### PDC_UNIX_OUT_SOCKET

Location of the Unix socket, to which messages from the Mediator are going to be forwarded to.

    PDC_UNIX_OUT_SOCKET=/tmp/unix_out.sock

### PDC_UNIX_IN_SOCKET

Location of the Unix socket, that accepts incoming messages from connected applications.

    PDC_UNIX_IN_SOCKET=/tmp/unix_in.sock

### PDC_SESSION_STATE

Location of a JSON file, that stores a temporary state.

    PDC_SESSION_STATE=/tmp/mc_state.json

This is done to protect from data loss, when restarting the MediatorConnect process. When a DisplayServer connects to the Mediator, the Mediator sends session definition of all sessions that are currently running. Those definitions are incomplete. For that reason, whenever a new session is started, we store the complete definition in this state file. This ensures that, whenever the MediatorConnect process is restarted, we can take the complete session definition from this file, instead of relying on the incomplete definitions that are sent from the Mediator.

### PDC_LOG_DIR

Location of an existing directory, to which the MediatorConnect process writes its log files.

    PDC_LOG_DIR=/tmp/mediator_logs

### PDC_BLACKLIST

A list (comma-separated) of session titles, that are going to be ignored.

    PDC_BLACKLIST=Single translation

This can be useful when trying to exclude sessions that are very short and can occur very frequently, like "Single translation" sessions, that are sessions that request a translation of a single sentence or expression.
